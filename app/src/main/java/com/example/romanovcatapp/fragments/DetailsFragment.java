package com.example.romanovcatapp.fragments;

        import android.os.Bundle;
        import android.view.LayoutInflater;
        import android.view.View;
        import android.view.ViewGroup;
        import android.widget.TextView;

        import androidx.annotation.NonNull;
        import androidx.annotation.Nullable;
        import androidx.fragment.app.Fragment;

        import com.example.romanovcatapp.R;
        import com.example.romanovcatapp.models.Breed;
        import com.google.gson.Gson;

public class DetailsFragment extends Fragment {

    private static final String ARG_PARAM1 ="param1";
    private String mParam1;

    public DetailsFragment(){

    }

    public static DetailsFragment newInstance(String param1){
        DetailsFragment fragment= new DetailsFragment();
        Bundle args = new Bundle();
        args.putString(ARG_PARAM1,param1);
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getArguments()!=null){
            mParam1=getArguments().getString(ARG_PARAM1);
        }
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        Gson g = new Gson();
        Breed breed = g.fromJson(mParam1, Breed.class);
        View v = inflater.inflate(R.layout.fragment_details, container, false);

        TextView name = v.findViewById(R.id.name);
        name.setText(breed.getName());

        TextView id = v.findViewById(R.id.id);
        id.setText("id: " + breed.getId());

        TextView description = v.findViewById(R.id.description);
        description.setText(breed.getDescription());

        TextView temperament = v.findViewById(R.id.characteristics);
        temperament.setText(breed.getTemperament());

        TextView country = v.findViewById(R.id.country);
        country.setText(breed.getOrigin());

        TextView weight = v.findViewById(R.id.weight);
        weight.setText(breed.getWeight().getMetric() + " kgs");

        TextView lifeSpan = v.findViewById(R.id.life);
        lifeSpan.setText(breed.getLife_span() + " average life span");

        return v;
    }
}
