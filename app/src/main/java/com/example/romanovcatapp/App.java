package com.example.romanovcatapp;

import android.app.Application;
import android.content.res.Configuration;


import com.example.romanovcatapp.utils.APIService;
import com.example.romanovcatapp.utils.Constants;
import com.google.gson.Gson;
import com.google.gson.GsonBuilder;

import okhttp3.OkHttpClient;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class App extends Application {
    private static APIService apiService;
    private  Retrofit retrofit;
    private Gson gson = new GsonBuilder().setLenient().create();

    @Override
    public void onCreate() {
        super.onCreate();

        retrofit=new Retrofit.Builder()
                .baseUrl(Constants.BaseURL)
                .client(new OkHttpClient.Builder().build())
                .addConverterFactory(GsonConverterFactory.create(gson))
                .build();
        apiService = retrofit.create(APIService.class);
    }
    public static APIService getApi(){
        return apiService;
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}

