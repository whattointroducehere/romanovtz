package com.example.romanovcatapp.utils;

public interface ConnectionProblem {
    void onProblem();
}
