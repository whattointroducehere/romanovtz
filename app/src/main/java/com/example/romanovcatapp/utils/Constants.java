package com.example.romanovcatapp.utils;

public class Constants {
    public static final String BaseURL="https://api.thecatapi.com";

    public static final String BreedsApi= "/v1/breeds";
}
