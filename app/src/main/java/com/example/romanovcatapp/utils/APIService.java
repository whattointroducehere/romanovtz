package com.example.romanovcatapp.utils;

import com.example.romanovcatapp.models.Breed;

import java.util.List;

import retrofit2.Call;
import retrofit2.http.GET;

public interface APIService {
    @GET(Constants.BreedsApi)
    Call<List<Breed>> getBreeds();
}
